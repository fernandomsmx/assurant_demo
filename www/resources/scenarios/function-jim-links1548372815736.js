(function(window, undefined) {

  var jimLinks = {
    "6087a915-32bb-4c1c-9ad1-7ac79827882d" : {
      "Rectangle_1" : [
        "ed4d4e83-4403-407c-adb1-3c8d1fda93a7"
      ],
      "Rectangle_2" : [
        "ed4d4e83-4403-407c-adb1-3c8d1fda93a7"
      ],
      "Rectangle_3" : [
        "ed4d4e83-4403-407c-adb1-3c8d1fda93a7"
      ],
      "raised_Button" : [
        "4aef302e-d2a1-437c-baa7-87c76e9368f4"
      ],
      "Button_11" : [
        "c3a13c16-4476-4c0c-8630-b0437e43d1bf"
      ]
    },
    "5fa1190a-89bd-49ee-878f-9d2cac2d6cb1" : {
      "raised_Button" : [
        "7cebd357-a097-4bdc-8139-a0539d53eac0"
      ]
    },
    "83b457c8-4d75-4b43-8d03-b50439ad0037" : {
      "raised_Button" : [
        "5fa1190a-89bd-49ee-878f-9d2cac2d6cb1"
      ],
      "Cell_6" : [
        "aac629e4-7911-4c02-8529-971e4292904f"
      ],
      "Rectangle_2" : [
        "29fc2a75-b1dd-471c-b13d-5e966461e6b4"
      ],
      "Rectangle_1" : [
        "c3a13c16-4476-4c0c-8630-b0437e43d1bf"
      ]
    },
    "e676a5d6-67ca-4e44-a158-532465816fb1" : {
      "raised_Button" : [
        "6087a915-32bb-4c1c-9ad1-7ac79827882d"
      ]
    },
    "aac629e4-7911-4c02-8529-971e4292904f" : {
      "Rectangle_15" : [
        "83b457c8-4d75-4b43-8d03-b50439ad0037"
      ],
      "Rectangle_14" : [
        "c3a13c16-4476-4c0c-8630-b0437e43d1bf"
      ]
    },
    "4aef302e-d2a1-437c-baa7-87c76e9368f4" : {
      "raised_Button_1" : [
        "e676a5d6-67ca-4e44-a158-532465816fb1"
      ]
    },
    "c3a13c16-4476-4c0c-8630-b0437e43d1bf" : {
      "Image_3" : [
        "83b457c8-4d75-4b43-8d03-b50439ad0037"
      ],
      "Rectangle_1" : [
        "aac629e4-7911-4c02-8529-971e4292904f"
      ],
      "Image_4" : [
        "83b457c8-4d75-4b43-8d03-b50439ad0037"
      ],
      "Image_5" : [
        "83b457c8-4d75-4b43-8d03-b50439ad0037"
      ],
      "Image_6" : [
        "83b457c8-4d75-4b43-8d03-b50439ad0037"
      ]
    },
    "7cebd357-a097-4bdc-8139-a0539d53eac0" : {
      "raised_Button" : [
        "6087a915-32bb-4c1c-9ad1-7ac79827882d"
      ]
    },
    "ed4d4e83-4403-407c-adb1-3c8d1fda93a7" : {
      "Ellipse_273" : [
        "6087a915-32bb-4c1c-9ad1-7ac79827882d"
      ]
    },
    "d12245cc-1680-458d-89dd-4f0d7fb22724" : {
      "raised_Button" : [
        "c3a13c16-4476-4c0c-8630-b0437e43d1bf"
      ]
    },
    "29fc2a75-b1dd-471c-b13d-5e966461e6b4" : {
      "Rectangle_2" : [
        "c3a13c16-4476-4c0c-8630-b0437e43d1bf"
      ],
      "Rectangle_3" : [
        "83b457c8-4d75-4b43-8d03-b50439ad0037"
      ],
      "Rectangle_1" : [
        "83b457c8-4d75-4b43-8d03-b50439ad0037"
      ]
    },
    "b94e8560-b3de-44c1-9939-5864aba299a7" : {
      "Rectangle_1" : [
        "83b457c8-4d75-4b43-8d03-b50439ad0037"
      ],
      "Rectangle_6" : [
        "83b457c8-4d75-4b43-8d03-b50439ad0037"
      ],
      "Rectangle_7" : [
        "83b457c8-4d75-4b43-8d03-b50439ad0037"
      ],
      "Rectangle_2" : [
        "c3a13c16-4476-4c0c-8630-b0437e43d1bf"
      ]
    },
    "3ba8d5bd-feb8-4695-a80e-39bcebebe7eb" : {
      "arrow-back_4" : [
        "c3a13c16-4476-4c0c-8630-b0437e43d1bf"
      ]
    },
    "f39803f7-df02-4169-93eb-7547fb8c961a" : {
      "Two-line-item_24" : [
        "aac629e4-7911-4c02-8529-971e4292904f"
      ],
      "Two-line-item_34" : [
        "b94e8560-b3de-44c1-9939-5864aba299a7"
      ],
      "Two-line-item_12" : [
        "29fc2a75-b1dd-471c-b13d-5e966461e6b4"
      ],
      "Two-line-item_44" : [
        "3ba8d5bd-feb8-4695-a80e-39bcebebe7eb"
      ],
      "Two-line-item_45" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);