(function(window, undefined) {
  var dictionary = {
    "6087a915-32bb-4c1c-9ad1-7ac79827882d": "Reporte",
    "5fa1190a-89bd-49ee-878f-9d2cac2d6cb1": "Trayecto",
    "83b457c8-4d75-4b43-8d03-b50439ad0037": "Detalle",
    "e676a5d6-67ca-4e44-a158-532465816fb1": "Rating",
    "aac629e4-7911-4c02-8529-971e4292904f": "Cal Mes",
    "4aef302e-d2a1-437c-baa7-87c76e9368f4": "Firma",
    "c3a13c16-4476-4c0c-8630-b0437e43d1bf": "Mapa",
    "7cebd357-a097-4bdc-8139-a0539d53eac0": "Servicio",
    "ed4d4e83-4403-407c-adb1-3c8d1fda93a7": "Camara",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "Login",
    "29fc2a75-b1dd-471c-b13d-5e966461e6b4": "Cal dia",
    "b94e8560-b3de-44c1-9939-5864aba299a7": "Cal Sem",
    "3ba8d5bd-feb8-4695-a80e-39bcebebe7eb": "Chat",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);